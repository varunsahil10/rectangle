module.exports = (x,y,callbackwalafunction) => {// ek function object export kr rha h
	if (x<=0 || y<=0) {
		setTimeout(()=>
			callbackwalafunction(new Error("Galat input h"),null) //Error(keyword h) ek object banaya h humne
			,2000);
	}
	else{
		setTimeout(()=>
			callbackwalafunction(null,{
				//object pass ho rha h
				perimeter: ()=>(2*(x+y)),
				area: ()=>(x*y)
			})
			,5000);
	}
}